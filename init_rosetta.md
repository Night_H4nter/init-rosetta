# Init Rosetta
**Regardless of what init system and what operating system you use, please make
sure to read the [notes](#notes) below.**

| what to do/init system       | systemd                            | openRC                                                                       | runit | s6 | suite66 | dinit | 
|------------------------------|------------------------------------|------------------------------------------------------------------------------|-------|----|---------|-------|
| list service files           | `systemctl list-unit-files`        | `rc-status`                                                                  |       | `ls [SERVICE_REPO]` | `66-intree` | `ls /etc/dinit.d`|
| list running services status | `systemctl list-units`             | `rc-update show`                                                             | sv status | `s6-rc -a list` | `66-intree -go contents`; see [notes](#general-1) | `dinitctl list`; see [notes](#dinit)|
| list failed services         | `systemctl --failed`               | `rc-status --crashed`                                                        |       | N/A; see [notes](#general) | `66-intree -go contents`; see [notes](#general-1) | `dinitctl list`; see [notes](#dinit) |
| list available services      | `systemctl --all`                  | `rc-update -v show` or `ls /etc/init.d/`                                     | `ls /etc/sv/` (list all enabled services: `ls /etc/run/service`) | `s6-rc -d list` | `ls [FRONTEND_LOCATION]/service`; see [notes](#for-artix-users-2) for Artix | `dinitctl list`; see [notes](#dinit)   |
| start a service              | `systemctl start [SERVICE_NAME]`   | `rc-service [SERVICE_NAME] start` or `/etc/init.d/[SERVICE_NAME] start`      | `sv up [SERVICE_NAME]` or `sv u [SERVICE_NAME]` | `s6-rc -u change [SERVICE_NAME]` or `s6-rc start [SERVICE_NAME]` | `66-start [SERVICE_NAME]` | `dinitctl start [SERVICE_NAME]`|
| stop a service               | `systemctl stop [SERVICE_NAME]`    | `rc-service [SERVICE_NAME] stop` or `/etc/init.d/[SERVICE_NAME] stop`        | `sv down [SERVICE_NAME]` or `sv d [SERVICE_NAME]` | `s6-rc -d change [SERVICE_NAME]` or `s6-rc stop [SERVICE_NAME]` | `66-stop [SERVICE_NAME]` | `dinitctl stop [SERVICE_NAME]`     |
| restart a service            | `systemctl restart [SERVICE_NAME]` | `rc-service [SERVICE_NAME] restart` or `/etc/init.d/[SERVICE_NAME] restart`  | `sv restart [SERVICE_NAME]` or `sv t [SERVICE_NAME]` | `s6-svc -r [SCANDIR]/[SERVICE_NAME]` *(for long running services only)*| *no standalone command - stop and start again* | `dinitctl restart [SERVICE_NAME]`     |
| get the status of a service  | `systemctl status [SERVICE_NAME]`  | `rc-service [SERVICE_NAME] status` or `/etc/init.d/[SERVICE_NAME] status`    | `sv status [SERVICE_NAME]` or `sv s [SERVICE_NAME]` | `s6-rc -a list \| grep -qFx [SERVICE_NAME]`; exits 0, if `[SERVICE_NAME]` is up, exits 1 otherwise | `66-inservice [SERVICE_NAME]` | `dinitctl list`; see [notes](#dinit)     |
| enable a service             | `systemctl enable [SERVICE_NAME]`  | `rc-update add [SERVICE_NAME]`                                               | `ln -s /etc/sv/[SERVICE_NAME] /var/service/` | `export [VAR]=$(s6-rc-db -c [DATABASE] contents [BUNDLE]) && s6-rc-bundle -f add [BUNDLE] $[VAR] [SERVICE_NAME]`; see [notes](#for-artix-users-1) for Artix  | `66-enable [SERVICE_NAME]` | `dinitctl enable [SERVICE_NAME]` |
| disable a service            | `systemctl disable [SERVICE_NAME]` | `rc-update del [SERVICE_NAME]`                                               | `rm /var/service/[SERVICE_NAME]` | `export [VAR]=$(s6-rc-db -c [DATABASE] contents [BUNDLE]) && s6-rc-bundle -f add [BUNDLE] $(echo $[VAR] \| sed 's/[SERVICE_NAME]//g')`; see [notes](#for-artix-users-1) for Artix  | `66-disable [SERVICE_NAME]` | `dinitctl disable [SERVICE_NAME]`     |


# Notes

#### runit

##### For Artix users

* available services are in `/etc/runit/sv/` directory 
* enabled services are in `/run/runit/service/` directory


#### s6

##### General

* most of the commands listed are actually commands provided by `s6-rc`, which
works on top of `s6`; you should generally avoid touching `s6` itself, unless
it's suggested here, or you really know what you're doing
* `s6-rc` does not implement "failed" state; only "up" and "down" states are
supported; if the service is down, but should be up, read the logs
* when disabling a service, keep in mind you should also disable all the 
services that depend on it, otherwise the service manager will restart the
disabled service as a dependency of another service
* all the commands suggested for manually enabling/disabling a service do not 
make any sanity checks, so make sure you've done all the necessary backups
before running them
   * do not use the variable name that is already in use by something else
   * after running suggested commands, you'll still have the previous contents
   of your database stored in `[VAR]`; so you can recover from that

- `[SERVICE_REPO]` is the absolute path to your service repository
for `s6-svscan`
    * refer to your system documentation, if you don't know this path
- `[SCANDIR]` is the absolute path to your scan directory for `s6-svscan`
    * run `pstree -a | head -n1`; the last argument of command in the output
    should be the path you  need
- `[VAR]` is an arbitrary variable to store previous contents of the bundle
- `[DATABASE]` is the absolute path to the compiled database
    * refer to your system documentation, if you don't know this path
- `[BUNDLE]` is the name of bundle, most likely the top level one
    * refer to your system documentation, if you don't know the bundle name
    * issuing `s6-rc-db -d all-dependencies [SERVICE_NAME]` might help, as it 
    lists all the services that depend on `[SERVICE_NAME]`, the top level
    bundle included, you can try making something up from there


##### For Artix users

* all commands listed require `sudo` privileges by default 
* to enable/disable a service, touch/rm a file with corresponding name
in `/etc/s6/adminsv/default/contents.d/`, and run `s6-db-reload`
```sh
touch /etc/s6/adminsv/default/contents.d/[SERVICE_NAME]; s6-db-reload  #enable 
rm    /etc/s6/adminsv/default/contents.d/[SERVICE_NAME]; s6-db-reload  #disable 
```

if the `s6-db-reload` command fails, or gives you warnings, you can compile
the s6 database manually and then switch your system to it:
```sh
TS="$(date +%s)"
s6-rc-compile /etc/s6/rc/compiled-$TS /etc/s6/adminsv /etc/s6/sv
s6-rc-update /etc/s6/rc/compiled-$TS
rm /etc/s6/rc/compiled
ln -s /etc/s6/rc/compiled-$TS /etc/s6/rc/compiled
```

- `[SERVICE_REPO]` is `/etc/s6/sv/` directory
- `[SCANDIR]` is `/run/service/` directory
- `[VAR]` is an arbitrary variable to store previous contents of the bundle
- `[DATABASE]` is `/run/s6-rc/compiled/` directory
- `[BUNDLE]` is then name of bundle, most likely `default`

#### suite66

##### General

* Like `s6-rc`, `66-*` are wrappers for `s6`. Same warnings apply:
don't touch `s6-*` without knowing what you're doing.
* Suite 66 has support for user services; running `66-*` as a normal user will
show the contents of your ~/.66 directory.
* The output of `66-intree -go contents` is as follows: `(PID,enabled,type) name`  
For `oneshot`-type services, PID can be `uninitialized` (not yet run), `down`
(did not finish successfully) or `up` (finished successfully). For other types,
a PID of zero means that a service isn't running. A PID ≥ 1 means that
a service is running, and has the specified PID. If a service isn't running,
but should be, read the logs to see how/if it failed.

##### For Artix users

**Note: Artix has dropped suite66 support a while ago. It may change again
in the future, but as of 19.04.2023, Artix doesn't support suite66.**

* On Artix, `[FRONTEND_LOCATION]` can be `/etc/66/` (packaged service files)
or `/etc/suite66/` (user-supplied service files, higher priority).

#### Dinit

##### General
* `dinitctl` has a nice list with various symbols which indicates the status of a service; below is a copy'n'paste of the man page regarding these afformentioned symbols:
```
[{+}     ] — service has started.
[{ }<<   ] — service is starting.
[   <<{ }] — service is starting, will stop once started.
[{ }>>   ] — service is stopping, will start once stopped.
[   >>{ }] — service is stopping.
[     {-}] — service has stopped.
```
* The << and >> symbols represent a transition state (starting and stopping
respectively); curly braces indicate the target state  (left:  started,
right: stopped); square brackets are used if the service is marked active
(target state will always be started if this is the case).
* An  's'  in place of '+' means that the service startup was skipped (possible
only if the service is configured as skippable). An 'X' in place of '-' means
that the service failed to start, or that the service process unexpectedly
terminated with an error status or signal while running. * Additional
information, if available, will be printed after the service name: whether
the service owns, or is waiting to  acquire,  the  console; the process ID;
the exit status or signal that caused termination.
* `dinit` becomes aware of service definitions only as they're requested,
either by the user or as dependencies - `dinitctl list` will, therefore, only
list services which have been started at least once.
* Dinit supports user services, so if run without root, it'll look for services
in `~/dinit.d` and look for a user instance of `dinit` to communicate with.
Use `sudo dinitctl` to control the system instance.

